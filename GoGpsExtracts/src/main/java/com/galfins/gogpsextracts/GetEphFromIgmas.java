package com.galfins.gogpsextracts;

import android.location.Location;

import com.galfins.gogpsextracts.Bds.BdsEphemeris;
import com.galfins.gogpsextracts.Bds.RinexNavigationParserBds;
import com.galfins.gogpsextracts.Gal.GalEphemeris;
import com.galfins.gogpsextracts.Gal.RinexNavigationParserGalileo;
import com.galfins.gogpsextracts.Gps.GpsEphemeris;
import com.galfins.gogpsextracts.Gps.RinexNavigationGps;
import com.galfins.gogpsextracts.Gps.RinexNavigationParserGps;
import com.alibaba.fastjson.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class GetEphFromIgmas {
    public static String UserName = "test";
    public static String PassWord = "test";
    public static String LoginPara = "userName=" + UserName + "&passWord=" + PassWord;
    public static String GetGPSEphUrl = "http://118.31.19.219:8080/api/getGPSEph?" + LoginPara;
    public static String GetBDSEphUrl = "http://118.31.19.219:8080/api/getBDSEph?" + LoginPara;
    public static String GetGALEphUrl = "http://118.31.19.219:8080/api/getGALEph?" + LoginPara;
    public static String GetGLOEphUrl = "http://118.31.19.219:8080/api/getGLOEph?" + LoginPara;

    public static void main(String s[]) {
        RinexNavigationGps rnp = new RinexNavigationGps();
        rnp.getRNPByTimestamp(123);
    }

    public static String httpGet(String httpurl) {
        HttpURLConnection connection = null;
        InputStream is = null;
        BufferedReader br = null;
        String result = null;// 返回结果字符串
        try {
            // 创建远程url连接对象
            URL url = new URL(httpurl);
            // 通过远程url连接对象打开一个连接，强转成httpURLConnection类
            connection = (HttpURLConnection) url.openConnection();
            // 设置连接方式：get
            connection.setRequestMethod("GET");
            // 设置连接主机服务器的超时时间：15000毫秒
            connection.setConnectTimeout(15000);
            // 设置读取远程返回的数据时间：60000毫秒
            connection.setReadTimeout(60000);
            // 发送请求
            connection.connect();
            // 通过connection连接，获取输入流
            if (connection.getResponseCode() == 200) {
                is = connection.getInputStream();
                // 封装输入流is，并指定字符集
                br = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                // 存放数据
                StringBuffer sbf = new StringBuffer();
                String temp = null;
                while ((temp = br.readLine()) != null) {
                    sbf.append(temp);
                    sbf.append("\r\n");
                }
                result = sbf.toString();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 关闭资源
            if (null != br) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (null != is) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            connection.disconnect();// 关闭远程连接
        }
        return result;
    }
}
